CFLAGS += -std=gnu99 -O0 -g -Wall -fno-omit-frame-pointer
LDFLAGS += -pthread -g

guest-proxy: guest-proxy.o

host-proxy: host-proxy.o

clean:
	rm -f *.o guest-proxy host-proxy

