/*
 * Copyright 2019 University of Washington, Max Planck Institute for
 * Software Systems, and The University of Texas at Austin
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/mman.h>
#include <unistd.h>

#define PEERID_LOCAL 0
#define PEERID_VM 1

#define NOTIFY_FDS 5
#define INT_FDS 32

static int epfd;
void *shmptr;
static int uxfd;
static int shmfd;
static int nfd[NOTIFY_FDS];
static int intfd[INT_FDS];

static int uxsocket_init(const char *path)
{
  int fd;
  struct sockaddr_un saun;
  struct epoll_event ev;

  if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("uxsocket_init: socket failed");
    goto error_exit;
  }

  memset(&saun, 0, sizeof(saun));
  saun.sun_family = AF_UNIX;
  memcpy(saun.sun_path, path, strlen(path));
  if (bind(fd, (struct sockaddr *) &saun, sizeof(saun))) {
    perror("uxsocket_init: bind failed");
    goto error_close;
  }

  if (listen(fd, 5)) {
    perror("uxsocket_init: listen failed");
    goto error_close;
  }

  ev.events = EPOLLIN;
  ev.data.fd = -1;
  if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &ev) != 0) {
    perror("uxsocket_init: epoll_ctl listen failed");
    goto error_close;
  }

  uxfd = fd;
  return 0;

error_close:
  close(fd);
error_exit:
  return -1;
}

static int uxsocket_send(int connfd, int64_t payload, int fd)
{
  ssize_t tx;
  struct iovec iov = {
    .iov_base = &payload,
    .iov_len = sizeof(payload),
  };
  union {
    char buf[CMSG_SPACE(sizeof(int))];
    struct cmsghdr align;
  } u;
  struct msghdr msg = {
    .msg_name = NULL,
    .msg_namelen = 0,
    .msg_iov = &iov,
    .msg_iovlen = 1,
    .msg_control = u.buf,
    .msg_controllen = 0,
    .msg_flags = 0,
  };
  struct cmsghdr *cmsg = &u.align;

  if (fd >= 0) {
    msg.msg_controllen = sizeof(u.buf);

    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int));

    *(int *) CMSG_DATA(cmsg) = fd;
  }

  if((tx = sendmsg(connfd, &msg, 0)) != sizeof(int64_t)) {
    fprintf(stderr, "tx == %zd\n", tx);
    return -1;
  }

  return 0;
}

static int uxsocket_accept(int lfd)
{
  int cfd, i;

  /* new connection on unix socket */
  if ((cfd = accept(lfd, NULL, NULL)) < 0) {
    fprintf(stderr, "uxsocket_accept: accept failed\n");
    return -1;
  }

  printf("connection accepted: %d\n", cfd);

  /* send protocol version, peer id, and shared memory fd */
  uxsocket_send(cfd, 0, -1);
  uxsocket_send(cfd, PEERID_VM, -1);
  uxsocket_send(cfd, -1, shmfd);
  printf("initial message sent: %d\n", cfd);

  /* send our notify fds to peer */
  for (i = 0; i < NOTIFY_FDS; i++) {
    fprintf(stderr, "send notify fd: %d\n", i);
    uxsocket_send(cfd, PEERID_LOCAL, nfd[i]);
  }
  printf("notify fds sent: %d\n", cfd);

  /* send interrupt notify fds to peer */
  for (i = 0; i < INT_FDS; i++) {
    fprintf(stderr, "send int fd: %d\n", i);
    uxsocket_send(cfd, PEERID_VM, intfd[i]);
  }
  printf("interrupt fds sent: %d\n", cfd);

  return 0;
}

static int shm_create(const char *name, size_t size, void **addr)
{
  int fd;
  void *p;
  char path[128];

  snprintf(path, sizeof(path), "/dev/hugepages/%s", name);

  if ((fd = open(path, O_CREAT | O_RDWR, 0666)) == -1) {
    perror("util_create_shmsiszed: open failed");
    goto error_out;
  }
  if (ftruncate(fd, size) != 0) {
    perror("util_create_shmsiszed: ftruncate failed");
    goto error_remove;
  }

  if ((p = mmap(NULL, size, PROT_READ | PROT_WRITE,
      MAP_SHARED | MAP_POPULATE, fd, 0)) == (void *) -1)
  {
    perror("util_create_shmsiszed: mmap failed");
    goto error_remove;
  }

  memset(p, 0, size);

  *addr = p;
  return fd;

error_remove:
  close(fd);
  unlink(path);
error_out:
  return -1;
}

int main(int argc, char *argv[])
{
  int n, i;
  ssize_t ret;
  uint64_t x;
  struct epoll_event evs[32];
  volatile uint32_t *up;

  if ((shmfd = shm_create("tas-vm", 32 * 1024 * 1024, &shmptr)) < 0) {
    return EXIT_FAILURE;
  }

  up = (volatile uint32_t *) shmptr;

  if ((epfd = epoll_create1(0)) == -1) {
    perror("epoll_create1 failed");
    return EXIT_FAILURE;
  }

  if (uxsocket_init("/tmp/tas-vm") != 0) {
    return EXIT_FAILURE;
  }

  /* outgoing fds */
  for (i = 0; i < NOTIFY_FDS; i++) {
    if ((nfd[i] = eventfd(0, EFD_NONBLOCK)) < 0) {
      perror("eventfd failed");
      return EXIT_FAILURE;
    }

    struct epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = i;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, nfd[i], &ev) != 0) {
      perror("uxsocket_init: epoll_ctl listen failed");
      return EXIT_FAILURE;
    }
  }

  /* incoming fds */
  for (i = 0; i < INT_FDS; i++) {
    if ((intfd[i] = eventfd(0, EFD_NONBLOCK)) < 0) {
      perror("eventfd failed");
      return EXIT_FAILURE;
    }
  }

  printf("starting event loop\n");
  while (true) {
     n = epoll_wait(epfd, evs, 32, -1);
     for (i = 0; i < n; i++) {
       if (evs[i].data.fd == -1) {
         uxsocket_accept(uxfd);
       } else {
         printf("notification: %d\n", evs[i].data.fd);
         ret = read(nfd[evs[i].data.fd], &x, sizeof(x));

         uint32_t y = up[evs[i].data.fd];
         up[evs[i].data.fd] = 0;
         printf("  ret=%zd x=%lu mem[%d]=%u\n", ret, x, evs[i].data.fd, y);

         if (y < INT_FDS) {
           x = 1;
           write(intfd[y], &x, sizeof(x));
         }
       }
     }
  }

  close(uxfd);
  close(epfd);
  return 0;
}
