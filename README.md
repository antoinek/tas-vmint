Simplest design:
  - tas running on host unmodified
  - host proxy connecting to TAS as app and to qemu for ivshmem
  - qemu ivshmem
  - guest proxy mapping PCI device


Host Proxy:
  - peer id 0
  - N + 1 vectors for N TAS cores
