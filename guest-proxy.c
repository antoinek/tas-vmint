#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

#include <linux/vfio.h>

struct vfio_dev {
  int containerfd;
  int groupfd;
  int devfd;

  struct vfio_device_info info;
};

static int vfio_dev_open(struct vfio_dev *dev, const char *groupname,
    const char *pci_dev)
{
  int container, group, device, i;
  struct vfio_group_status group_status =
          { .argsz = sizeof(group_status) };

  /* Create a new container */
  if ((container = open("/dev/vfio/vfio", O_RDWR)) < 0) {
    perror("open vfio failed");
    goto out;
  }

  if (ioctl(container, VFIO_GET_API_VERSION) != VFIO_API_VERSION) {
    perror("unexpected vfio api version");
    goto out_container;
  }

  /* Open the group */
  group = open(groupname, O_RDWR);
  if (group < 0) {
    perror("open group failed");
    goto out_container;
  }

  /* Test the group is viable and available */
  if (ioctl(group, VFIO_GROUP_GET_STATUS, &group_status) < 0) {
    perror("ioctl get status failed");
    goto out_group;
  }

  if (!(group_status.flags & VFIO_GROUP_FLAGS_VIABLE)) {
    fprintf(stderr, "vfio group not viable\n");
    goto out_group;
  }

  /* Add the group to the container */
  if (ioctl(group, VFIO_GROUP_SET_CONTAINER, &container) < 0) {
    fprintf(stderr, "set container failed\n");
    goto out_group;
  }

  /* Enable the IOMMU model we want */
  if (ioctl(container, VFIO_SET_IOMMU, VFIO_NOIOMMU_IOMMU) < 0) {
    fprintf(stderr, "set iommu model failed\n");
    goto out_group;
  }

  /* Get a file descriptor for the device */
  if ((device = ioctl(group, VFIO_GROUP_GET_DEVICE_FD, pci_dev)) < 0) {
    perror("getting device failed");
    goto out_group;
  }

  /* Test and setup the device */
  memset(&dev->info, 0, sizeof(dev->info));
  dev->info.argsz = sizeof(dev->info);
  if ((i = ioctl(device, VFIO_DEVICE_GET_INFO, &dev->info)) < 0) {
    perror("getting device info failed");
    goto out_dev;
  }

  dev->containerfd = container;
  dev->groupfd = group;
  dev->devfd = device;
  return 0;

out_dev:
  close(device);
out_group:
  close(group);
out_container:
  close(container);
out:
  return -1;
}

static int vfio_dev_reset(struct vfio_dev *dev)
{
  if (ioctl(dev->devfd, VFIO_DEVICE_RESET) < 0) {
    perror("vfio_dev_reset: reset failed");
    return -1;
  }
  return 0;
}

static int vfio_irq_info(struct vfio_dev *dev, uint32_t index,
    struct vfio_irq_info *irq)
{
  memset(irq, 0, sizeof(*irq));

  irq->argsz = sizeof(*irq);
  irq->index = index;

  if (ioctl(dev->devfd, VFIO_DEVICE_GET_IRQ_INFO, irq) < 0) {
    perror("vfio_irq_info: get info failed");
    return -1;
  }

  return 0;
}

static int vfio_irq_eventfd(struct vfio_dev *dev, uint32_t index,
    uint32_t count, int *fds)
{
  uint32_t i;
  size_t sz;
  int *pfd;
  void *alloc;
  struct vfio_irq_set *info;

  sz = sizeof(struct vfio_irq_set) + count * sizeof(int);
  if ((alloc = calloc(1, sz)) == NULL) {
    perror("calloc failed");
    return -1;
  }

  pfd = (int *) ((uint8_t *) alloc + sizeof(struct vfio_irq_set));
  for (i = 0; i < count; i++) {
    if ((fds[i] = eventfd(0, EFD_NONBLOCK)) < 0) {
      perror("vfio_irq_eventfd: eventfd failed");
      free(alloc);
      return -1;
    }
    pfd[i] = fds[i];
  }

  info = alloc;
  info->argsz = sz;
  info->flags = VFIO_IRQ_SET_DATA_EVENTFD | VFIO_IRQ_SET_ACTION_TRIGGER;;
  info->index = index;
  info->start = 0;
  info->count = count;

  if (ioctl(dev->devfd, VFIO_DEVICE_SET_IRQS, info) < 0) {
    perror("vfio_irq_eventfd: set failed");
    return -1;
  }

  free(alloc);

  return 0;
}

static int vfio_region_info(struct vfio_dev *dev, uint32_t index,
    struct vfio_region_info *reg)
{
  memset(reg, 0, sizeof(*reg));

  reg->argsz = sizeof(*reg);
  reg->index = index;

  if (ioctl(dev->devfd, VFIO_DEVICE_GET_REGION_INFO, reg) < 0) {
    perror("vfio_region_info: get info failed");
    return -1;
  }

  return 0;
}

static int vfio_region_map(struct vfio_dev *dev, uint32_t index,
    void **addr, size_t *len)
{
  void *ret;
  struct vfio_region_info reg;

  if (vfio_region_info(dev, index, &reg) != 0) {
    return -1;
  }

  if ((ret = mmap(NULL, reg.size, PROT_READ | PROT_WRITE,
          MAP_SHARED | MAP_POPULATE, dev->devfd, reg.offset))
      == MAP_FAILED)
  {
    perror("vfio_region_map: mmap failed");
    return -1;
  }

  *addr = ret;
  *len = reg.size;

  return 0;
}

int main(int argc, char *argv[])
{
  int i, n, efd;
  int *intfds;
  ssize_t sz;
  struct vfio_dev dev;
  struct vfio_irq_info irq;
  struct vfio_region_info reg;
  void *mem_regs, *mem_data;
  size_t len_regs, len_data;

  if (vfio_dev_open(&dev, "/dev/vfio/noiommu-0", "0000:00:04.0") != 0) {
    return EXIT_FAILURE;
  }

  printf("num flags: %x\n", dev.info.flags);
  printf("num regions: %u\n", dev.info.num_regions);
  printf("num irqs: %u\n", dev.info.num_irqs);

  for (i = 0; i < dev.info.num_regions; i++) {
    if (vfio_region_info(&dev, i, &reg) != 0) {
      fprintf(stderr, "  region %d: getting region info failed\n", i);
      continue;
    }

    printf("  region %d: flags=%x index=%x size=%llx offset=%llx\n", i, reg.flags,
        reg.index, reg.size, reg.offset);
  }

  for (i = 0; i < dev.info.num_irqs; i++) {
    if (vfio_irq_info(&dev, i, &irq) != 0) {
      fprintf(stderr, "  irq %d: getting irq info failed\n", i);
      continue;
    }

    printf("  irq %d: flags=%x index=%u count=%u\n", i, irq.flags, irq.index, irq.count);
  }


  vfio_dev_reset(&dev);

  vfio_region_map(&dev, 0, &mem_regs, &len_regs);
  vfio_region_map(&dev, 2, &mem_data, &len_data);

  if ((efd = epoll_create1(0)) < 0) {
    perror("epoll_create1 failed");
    return EXIT_FAILURE;
  }

  vfio_irq_info(&dev, 2, &irq);
  intfds = calloc(irq.count, sizeof(*intfds));
  if (vfio_irq_eventfd(&dev, 2, irq.count, intfds) != 0) {
    fprintf(stderr, "setting up irqs failed\n");
    exit(EXIT_FAILURE);
  }
  for (i = 0; i < irq.count; i++) {
    struct epoll_event evt;
    evt.events = EPOLLIN;
    evt.data.fd = i;
    if (epoll_ctl(efd, EPOLL_CTL_ADD, intfds[i], &evt) != 0) {
      perror("epoll_ctl failed");
    }
  }


  volatile uint32_t *shmp = mem_data;
  shmp[0] = 4;
  shmp[0] = 8;

  volatile uint32_t *dbp = (volatile uint32_t *) ((uint8_t *) mem_regs + 12);
  *dbp = 0;
  *dbp = 1;

  struct epoll_event evts[8];
  while (1) {
    if ((n = epoll_wait(efd, evts, 8, -1)) < 0) {
      perror("epoll_wait failed");
      exit(0);
    }

    for (i = 0; i < n; i++) {
      printf("interrupt: %d\n", evts[i].data.fd);
      uint64_t x;
      if ((sz = read(intfds[evts[i].data.fd], &x, sizeof(x))) < 0) {
        perror("read failed");
      }
    }
  }

  return 0;
}
